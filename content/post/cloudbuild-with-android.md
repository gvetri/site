+++
author = "Giuseppe Vetri"
title = "Cloudbuild with Android"
date = "2020-06-06"
description = "How to use cloudbuild with Android"
tags = [
    "Cloudbuild",
    "Android",
    "CI/CD"
]
+++

# New blog post available

In my blog [Codingpizza](www.codingpizza.com), I wrote about how you can use GCP Cloudbuild to build your android apps.
