+++
author = "Giuseppe Vetri"
title = "Codingpizza is now Streaming in Twitch"
date = "2020-09-10"
description = "I started to stream some talks and live coding sessions on Twitch."
tags = [
    "Android",
    "Kotlin"
]
+++

# We're streaming now.

Hi folks, I started streaming talks about Kotlin and Android on my Twitch channel. 
[Twitch.tv/codingpizza](www.twitch.tv/codingpizza), also I am doing some live coding. At this moment, the streaming is only in Spanish, so if you speak Spanish, come to watch it and join the community.