+++
author = "Giuseppe Vetri"
title = "Here's my first talk about how to use Kotlin DSL with Gradle. "
date = "2020-10-21"
description = "In this talk, I explain how to migrate from Groovy to Kotlin DSL, your Android Projects with Gradle. I talk about the pro and cons of Kotlin DSL."
tags = [
    "Android",
    "Kotlin"
]
+++

# New talk available.

Hi Folks, In the last week, I gave a talk about how to migrate from Groovy to Kotlin DSL, your Android Projects with Gradle. I talked about the pro and cons of Kotlin DSL and recommended some plugins that, if you use it with Kotlin DSL, can give your Android project superpowers. The talk is in Spanish, and you can found it at [this link.](https://youtu.be/kp-a2UNReY8?t=3081).



## This is the full event video.
{{< youtube kp-a2UNReY8 >}}
