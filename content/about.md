+++
title = "About"
description = "Hi, nice to meet you my name is Giuseppe Vetri"
date = "2020-08-02"
aliases = ["about-me"]
author = "Giuseppe Vetri"
+++

# Hi, nice to meet you. My name is Giuseppe Vetri.

I've worked as an Android Developer for the last four years in consultancy and startups. I like to learn new things, create something new with that knowledge, and share it.

I created some apps using Dart, Java, and Kotlin. I have a blog in which I talk about Android Development topics.  If you want to check it out, it's **[Codingpizza](https://www.codingpizza.com)**.





